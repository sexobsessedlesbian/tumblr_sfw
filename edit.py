#! /usr/bin/env python

import time

import utils

from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.keys import Keys

URL_FMT = 'https://www.tumblr.com/edit/%d'
TAG_FLAGGED = 'flagged'
POST_PRIVATELY = 'Post privately'
REDIRECT_URL = 'https://www.tumblr.com/dashboard'


def edit_posts(driver, pids):
    for pid in pids:
        edit_post(driver, pid)


def edit_post(driver, pid):
    url = URL_FMT % pid
    driver.get(url)

    time.sleep(0.1)
    tag_editor = driver.find_element_by_css_selector('div.post-form--tag-editor')
    tags = tag_editor.find_elements_by_css_selector('span.tag-label')

    for tag in tags:
        if tag.text == 'flagged':
            # already processed, nothing to do
            print '-- post %d already processed' % pid
            return

    input_tag_flagged(tag_editor)

    dropdown = driver.find_element_by_css_selector('div.post-form--save-button div.dropdown-area')
    dropdown.click()
    time.sleep(0.1)
    post_privately = post_privately_button(driver)
    post_privately.click()
    time.sleep(0.1)
    save = driver.find_element_by_css_selector('div.post-form--save-button button.button-area')
    save.click()

    # wait until it goes through
    wait_until_save_button_dne(driver)
    time.sleep(0.1)


def input_tag_flagged(parent):
    inp = parent.find_element_by_css_selector('div.editor-plaintext')
    inp.send_keys(TAG_FLAGGED)
    inp.send_keys(Keys.ENTER)


def post_privately_button(parent):
    list_items = parent.find_elements_by_css_selector('div.popover--save-post-dropdown li.item-option')
    if not list_items:
        raise NoSuchElementException('No elements found matching `div.popover--save-post-dropdown li.item-option`')
    for li in list_items:
        if li.text == POST_PRIVATELY:
            return li

    raise NoSuchElementException('Could not find `Post privately` button')


def wait_until_save_button_dne(parent):
    utils.wait_until_elem_dne(parent, 'div.post-form--save-button')
