# tumblr_sfw

So you have a smutty, smutty Tumblr and wand to clean it up, huh?

This script is here to help.

## Requirements
* python 2.x
* [geckodriver](https://github.com/mozilla/geckodriver/releases) (the Firefox driver), accessible from your $PATH

## Set Up
* pull down this repo (into a virtualenv, if that's your speed)
* install the requirements: `pip install -r requirements.txt`
* install [geckodriver](https://github.com/mozilla/geckodriver/releases) (the Firefox driver) and make sure it's accessible from your $PATH
* in your Tumblr Settings > Dashboard, disable endless scrolling (this script relies on pagination)
![how to disable endless scrolling](/static/disable-endless-scrolling.png "Disable endless scrolling")
* grab the cookies Tumblr sends when you're logged into your account, and export them to your environment as `$TUMBLR_COOKIES`
    * use Developer Tools > Network to snoop on a request while you're logged in, and grab all the cookies in that request
![how to get tumblr's cookies via Developer Tools](/static/grab-cookies.png "Grab the cookies")
    * in (say) your `~/.SECRET` file:
    ```export TUMBLR_COOKIES='that stuff you copied'```
    * and then source your file: `source ~/.SECRET`
* invoke the script, passing your blog name: e.g. I would run `./tumblr_sfw.py sex-obsessed-lesbian`
    * for more details, run `./tumblr_sfw.py --help`
