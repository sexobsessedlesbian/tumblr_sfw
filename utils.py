import os

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait


LONG_TIMEOUT = 15
RETRIES = 3


def find_elem_if_exists(parent, selector):
    try:
        return parent.find_element_by_css_selector(selector)
    except NoSuchElementException:
        return None


def wait_until_elem_dne(parent, selector):
    WebDriverWait(parent, LONG_TIMEOUT).\
        until_not(EC.presence_of_element_located((By.CSS_SELECTOR, selector)))


def new_driver_with_auth():
    driver = webdriver.Firefox()
    driver.implicitly_wait(5)
    driver.get('https://www.tumblr.com/')

    cookies_str = os.environ['TUMBLR_COOKIES']
    cookies = cookies_str.split('; ')
    for c in cookies:
        split = c.split('=', 1)
        driver.add_cookie({'name': split[0], 'value': split[1]})

    return driver
