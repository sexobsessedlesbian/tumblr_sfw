#! /usr/bin/env python

import argparse

from selenium.webdriver.common.by import By

from edit import edit_posts
import utils


class NoPostsFound(Exception):
    pass


def argument_parser():
    parser = argparse.ArgumentParser(description="""Make your Tumblr SFW again. At least as far as robots are concerned.

This script goes through your blog history one page at a time (www.tumblr.com/blog/<yourblog>/n),
finds flagged posts, and sets them to "private". It also tags them #flagged
so you can go back through and see what was hushed up.""")

    parser.add_argument(
        'blog',
        type=str,
        help='the name of your blog (e.g. if your url is "sexy-stuff.tumblr.com", pass "sexy-stuff")'
    )

    parser.add_argument(
        '--end',
        type=int,
        default=float('inf'),
        help='last page of blog history to process, inclusive (i.e. "www.tumblr.com/blog/<yourblog>/n")'
    )

    parser.add_argument(
        '--start',
        type=int,
        default=1,
        help='first page of blog history to process (i.e. "www.tumblr.com/blog/<yourblog>/n")'
    )

    return parser


def get_flagged_posts_for_page(driver, blog, page_num):
    url = 'https://www.tumblr.com/blog/%s/%d' % (blog, page_num)
    print 'Looking for flagged posts on %s' % url
    driver.get(url)

    pids = []

    flag_divs = driver.find_elements_by_css_selector('div.x-banner')
    for f in flag_divs:

        post = f.find_element(By.XPATH, "./ancestor::div[contains(@class, 'post')]")
        pid = post.get_attribute('data-id')

        private_label = utils.find_elem_if_exists(post, 'span.private_label')
        if private_label:
            # post has already been set to private, don't worry about it
            continue

        pids.append(pid)

    if not pids:
        # Check if we've run out of blog to clean up.
        # (OK maybe it's dumb to check all the way down here, but it's expensive, we
        # don't want to do it every time, so only check if we didn't find any flagged posts
        no_posts = utils.find_elem_if_exists(driver, 'div.no_posts_found')
        if no_posts:
            # We catch this error and know to bail
            raise NoPostsFound

    return [int(pid) for pid in pids]


def main():
    parser = argument_parser()
    args = parser.parse_args()

    print 'Let\'s clean up Tumblr: %s\n' % args.blog

    driver = utils.new_driver_with_auth()

    cur_page = args.start
    while cur_page <= args.end:
        try:
            pids = get_flagged_posts_for_page(driver, args.blog, cur_page)
        except NoPostsFound:
            print 'Congrats, that\'s the end of your blog history!'
            break
        print '- found %d flagged posts' % len(pids)
        if pids:
            edit_posts(driver, pids)
        cur_page += 1


if __name__ == '__main__':
    main()
